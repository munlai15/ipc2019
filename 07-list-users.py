#!/usr/bin/python3
# -*- coding:utf-8-*-
#07-list-users.py [-f file]
#  Donat un file tipus /etc/passwd o stdin (amb aquest format) fer:
# * el constructor d'objectes *UnixUser* rep un strg amb la línia sencera 
#   tipus /etc/passwd.
# * llegir línia a línia cada usuari assignant-lo a un objecte 
#   UnixUser i afegir l’usuari a una llista d’usuaris UnixUser.
# * un cop completada la carrega de dades i amb la llista amb 
#   tots els usuaris, llistar per pantalla els usuaris recorrent la llista.

class UnixUser():
	""" 
	Classe UnixUser: prototipus de /etc/passwd
	   login:passwd:uid:gid:gecos:home:shell
	"""
	def __init__(self, user_linea):
		"Constructor objectes UnixUser"
		camp = user_linea.split(':')
		self.login=camp[0]
		self.passwd=camp[1]
		self.uid=int(camp[2])
		self.gid=int(camp[3])
		self.gecos=camp[4]
		self.home=camp[5]
		self.shell=camp[6]
		
	def show(self):
		"Mostrar les dades de l'usuari"
		print("login: %s uid: %d gid: %d" % (self.login,self.uid,self.gid))
		
	def __str__(self):
		"Funcio per retornar un string del objecte"
		return "%s %s %d %d %s %s %s" % (self.login,self.passwd,self.uid,self.gid,self.gecos,self.home,self.shell)


import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--file", type=str, dest="file",
                     default="/dev/stdin", metavar="file",
                     help="fichero a recorrer, por defecto stdin")
args=parser.parse_args()

print(args)

fileIn = open(args.file, 'r')
listUsers = []

# 1 populate cargar usuarios en una lista
for linea in fileIn:
	user = UnixUser(linea)
	listUsers.append(user)
print(listUsers)
fileIn.close()

# 2 recorrer esta lista y mostrarla
for user in listUsers:
    user.show()
