#!/usr/bin/python3
# -*- coding:utf-8-*-
# 10-count-by-group.py [-s gid | gname | nusers ] -u usuaris -g grups
# LListar els grups del sistema ordenats pel criteri de gname, gid o de 
# número d'usuaris.
# *Atenció* cal gestionar apropiadament la duplicitat dels usuaris en un grup.
# Requeriment: desar a la llista d'usuaris del grup tots aquells usuaris
# que hi pertanyin, sense duplicitats, tant com a grup principal com a 
# grup secundari.

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-s","--sort",type=str,help="métode d'ordenació",\
		dest="metode",metavar="metodesort",choices=["gid","gname","nusers"],\
		default="gid")
parser.add_argument("-u","--fileusers",type=str,\
        required=True,help="fitxer passwd a processar", metavar="fileusers")
parser.add_argument("-g","--filegroups",type=str,\
        required=True,help="fitxer groups a processar", metavar="filegroups")
args=parser.parse_args()
print(args)

dicGroups={}
listUsers=[]

class UnixUser():
	""" 
	Classe UnixUser: prototipus de /etc/passwd
	   login:passwd:uid:gid:gecos:home:shell
	"""
	def __init__(self, user_linea):
		"Constructor objectes UnixUser"
		camp=user_linea.split(':')
		self.login=camp[0]
		self.passwd=camp[1]
		self.uid=int(camp[2])
		self.gid=int(camp[3])
		self.gname=""
		if self.gid in dicGroups:
			self.gname=dicGroups[self.gid].gname
		self.gecos=camp[4]
		self.home=camp[5]
		self.shell=camp[6]
		
	def show(self):
		"Mostrar les dades de l'usuari"
		print("login: %s uid: %d gid: %d gname: %s" % (self.login,self.uid,self.gid,self.gname))
		
	def __str__(self):
		"Funcio per retornar un string del objecte"
		return "%s %s %d %d %s %s %s %s" % (self.login,self.passwd,self.uid,self.gid,self.gname,self.gecos,self.home,self.shell)

class UnixGroup:
	"""
	Classe UnixGroup: prototipus de /etc/group
		gname:passwd:gid:users
	"""
	def __init__(self, group_linea):
		"Constructor objectes UnixGroup"
		camp=group_linea.split(':')
		self.gname=camp[0]
		self.passwd=camp[1]
		self.gid=int(camp[2])
		self.listUsersStr = camp[3]
		self.listUsers=[]
		if self.listUsersStr[:-1]:
			self.listUsers = self.listUsersStr[:-1].split(",")
	
	def show(self):
		"Mostrar les dades del grup"
		print("gname:%s passwd:%s gid:%d users:%d" % (self.gname,self.passwd,self.gid,self.users))
	
	def __str__(self):
		"Funcio per retornar un string del objecte"
		return "%s %s %d %s" % (self.gname,self.passwd,self.gid,self.listUsers)

# Carreguem els grups a un diccionari
group=open(args.filegroups,"r")
for linea in group:
	grup=UnixGroup(linea)
	dicGroups[grup.gid]=grup
group.close()

# Carreguem els usuaris a una llista
passwd=open(args.fileusers,"r")
for linea in passwd:
	user = UnixUser(linea)
	listUsers.append(user)
	if user.gid in dicGroups:
		if user.login not in dicGroups[user.gid].listUsers:
			dicGroups[user.gid].listUsers.append(user.login)
passwd.close()

index=[]
if args.metode=="gid":
  index = [ k for k in dicGroups ]
elif args.metode=="gname":
  index = [ (dicGroups[k].gname,k) for k in dicGroups ]
elif args.metode=="nusers":
  index = [ (len(dicGroups[k].listUsers),k) for k in dicGroups ]
index.sort()

if args.metode=="gid":
  for k in index:
   print (dicGroups[k])
else:
  for g,k in index:
   print (dicGroups[k])
exit(0)



