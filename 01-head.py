#!/usr/bin/python3
# -*- coding:utf-8-*-
#01-head.py [file]
#Mostrar les deu primeres línies de file o stdin
import sys

LINIES=10
fluxe_fitxer=sys.stdin

if len(sys.argv) == 2:
	fluxe_fitxer=open(sys.argv[1],"r")

cont=0
for line in fluxe_fitxer:
	cont=cont+1
	print (line, end=" ")
	if cont==LINIES: break
fluxe_fitxer.close()
exit(0)
	

