#!/usr/bin/python3
# -*- coding:utf-8-*-
# 11-exemple-popen.py dir
# Crear un programa que executa un ls de un argument rebut i 
# mostra per stdout el que rep del popen. Utilitza subprocess.Popen.

import argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description=\
	"""Exemple popen""")
parser.add_argument("ruta",type=str,help="directori a llistar")
args=parser.parse_args()

command = ["ls", args.ruta]
pipeData = Popen(command,stdout=PIPE) #Popen es un constructor que crea la tuberia 
for line in pipeData.stdout:
	print(line.decode("utf-8"),end="")
exit(0)
