#!/usr/bin/python3
# -*- coding:utf-8-*-
# 19-exemple-execv.py
# Ídem anterior però ara el programa fill execula un “ls -la /”. Executa un nou 
# procés carregat amb execv. Aprofitar per veure les fiferents variants de *exec*.

import sys,os
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
  print("Programa Pare", os.getpid(), pid)
  sys.exit(0)

print("Programa fill", os.getpid(), pid)
#os.execv("/usr/bin/ls",["/usr/bin/ls","-ls","/"])
#os.execl("/usr/bin/ls","/usr/bin/ls","-ls","/")
#os.execlp("ls","ls","-ls","/")
#os.execvp("uname",["uname","-a"])
#os.execv("/usr/bin/bash",["/usr/bin/bash","/var/tmp/ipc/ipc2019/show.sh"])
os.execle("/usr/bin/bash","/usr/bin/bash","/var/tmp/ipc/ipc2019/show.sh",{"nom":"joan","edat":"25"})
print("Hasta lugo lucas!")
sys.exit(0)


# variable l: si el num de paràmetres és fixe
# variable v: si el num de paràmetres és variable
# variables amb p: PATH (rep entorn del proces pare)
# variables amb e: env  (li hem de passar el env)
