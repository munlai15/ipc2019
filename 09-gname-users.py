#!/usr/bin/python3
# -*- coding:utf-8-*-
# 09-gname-users.py   [-s login|gid|gname]  -u fileusers -g fileGroup
# Carregar en una llista a memòria els usuaris provinents d'un fitxer tipus
# /etc/passwd, usant objectes UnixUser, i llistar-los. Ordenar el llistat
# (stdout) segons el criteri login o el criteri gid o el gname.
#
#  Requeriments: primer carregar a un diccionari totes les dades de tots
#  els grups. Després carregar a una llista totes les dades dels usuaris.
#  Finalment ordenar i llistar.

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-s","--sort",type=str,help="métode d'ordenació",\
		dest="metode",metavar="metodesort",choices=["login","gid","gname"],\
		default="login")
parser.add_argument("-u","--fileusers",type=str,\
        required=True,help="fitxer passwd a processar", metavar="fileusers")
parser.add_argument("-g","--filegroups",type=str,\
        required=True,help="fitxer groups a processar", metavar="filegroups")
args=parser.parse_args()
print(args)

dicGroups={}

class UnixUser():
	""" 
	Classe UnixUser: prototipus de /etc/passwd
	   login:passwd:uid:gid:gecos:home:shell
	"""
	def __init__(self, user_linea):
		"Constructor objectes UnixUser"
		camp=user_linea.split(':')
		self.login=camp[0]
		self.passwd=camp[1]
		self.uid=int(camp[2])
		self.gid=int(camp[3])
		self.gname=""
		if self.gid in dicGroups:
			self.ganame=dicGroups[self.gid].gname
		self.gecos=camp[4]
		self.home=camp[5]
		self.shell=camp[6]
		
	def show(self):
		"Mostrar les dades de l'usuari"
		print("login: %s uid: %d gid: %d gname: %s" % (self.login,self.uid,self.gid,self.ganame))
		
	def __str__(self):
		"Funcio per retornar un string del objecte"
		return "%s %s %d %d %s %s %s %s" % (self.login,self.passwd,self.uid,self.gid,self.gname,self.gecos,self.home,self.shell)

class UnixGroup:
	"""
	Classe UnixGroup: prototipus de /etc/group
		gname:passwd:gid:users
	"""
	def __init__(self, group_linea):
		"Constructor objectes UnixGroup"
		camp=group_linea.split(':')
		self.gname=camp[0]
		self.passwd=camp[1]
		self.gid=int(camp[2])
		self.users=camp[3]
	
	def show(self):
		"Mostrar les dades del grup"
		print("gname:%s passwd:%s gid:%d users:%d" % (self.gname,self.passwd,self.gid,self.users))
	
	def __str__(self):
		"Funcio per retornar un string del objecte"
		return "%s %s %d %d" % (self.gname,self.passwd,self.gid,self.users)

def sort_login(user):
	"""
	Comparador logins
	"""
	return user.login

def sort_gid(user):
	"""
	Comparador gids
	"""
	return user.gid, user.login

def sort_gname(user):
	"""
	Comparador gnames
	"""
	return user.gname, user.login

group=open(args.filegroups,"r")
for linea in group:
	grup=UnixGroup(linea)
	dicGroups[grup.gid]=grup
group.close()

passwd=open(args.fileusers,"r")
listUsers=[]
for linea in passwd:
	user = UnixUser(linea)
	listUsers.append(user)
passwd.close()

if args.metode == "login":
	listUsers.sort(key=sort_login)
elif args.metode == "gid":
	listUsers.sort(key=sort_gid)
elif args.metode == "gname":
	listUsers.sort(key=sort_gname)

for user in listUsers:
    user.show()
