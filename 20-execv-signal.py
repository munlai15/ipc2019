#!/usr/bin/python3
# -*- coding:utf-8-*-
# 20-execv-signal.py
# Usant l'exemple execv programar un procés pare que llança un fil i finalitza.
# El procés fill executa amb execv el programa python *16-signal.py* al que li 
# passa un valor hardcoded de segons.

import sys,os
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
  print("Programa Pare", os.getpid(), pid)
  sys.exit(0)

print("Programa fill", os.getpid(), pid)
os.execv("/usr/bin/python",["/usr/bin/python","16-signal.py","60"])
print("Hasta lugo lucas!")
sys.exit(0)

