#!/usr/bin/python3
# -*- coding:utf-8-*-
# 23-daytime-server-one2one.py
# Ídem exercici anterior, generar un daytime-server que accepta múltiples clients
# correlatius, és a dir, un un cop finalitzat l'anteior: *One2one*.

import sys,socket
from subprocess import Popen, PIPE

HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# host i port per on escoltar al client
s.bind((HOST, PORT))

# fem que escolti sempre per a un num x de clients.
# tancará connexions individuals però estarà sempre esperant una nova
# petició
s.listen(1)
while True:
	conn, addr = s.accept()
	print("Connected by", addr)
	# fa el popen i envia al client la informació
	ordre = ["date"]
	pipeData = Popen(ordre,stdout=PIPE)
	for line in pipeData.stdout:
		conn.send(line)
	# tanca
	conn.close()
	
sys.exit(0)


